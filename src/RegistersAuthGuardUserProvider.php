<?php

namespace Dottystyle\Laravel\SSO;

use Dottystyle\Laravel\SSO\Contracts\UserProvider;

trait RegistersAuthGuardUserProvider
{
    /**
     * Register the auth guard user provider.
     * 
     * @return void
     */
    public function registerAuthGuardUserProvider()
    {
        return $this->app->singleton(UserProvider::class, function ($app) {
            return new AuthGuardUserProvider($app['auth']->guard());
        });
    }
    
    /**
     * Get the auth guard user provider instance.
     * 
     * @return \Dottystyle\Laravel\SSO\AuthGuardUserProvider
     */
    public function getAuthGuardUserProvider()
    {
        return $this->app->make(UserProvider::class);
    }
}