<?php

namespace Dottystyle\Laravel\SSO\Contracts;

use Dottystyle\Laravel\SSO\UserInfo;

interface BrokerUserProvider
{
    /**
     * Retrieve the corresponding user by the user info from SSO.
     * 
     * @param Dottystyle\Laravel\SSO\UserInfo $user
     * @return \Dottystyle\Laravel\SSO\Contracts\User
     */
    public function retrieveByUserInfo(UserInfo $user);
}