<?php

namespace Dottystyle\Laravel\SSO\Contracts;

interface Token
{
    /**
     * Get the token identifier.
     * 
     * @return string
     */
    public function getTokenId();

    /**
     * Get the user from token. 
     * 
     * @return mixed
     */
    public function getUserId();

    /**
     * Determine whether the SSO is expired or not.
     * 
     * @return boolean
     */
    public function expired();

    /**
     * Get the remaining minutes of the token.
     * 
     * @return int
     */
    public function remainingMinutes();
}