<?php

namespace Dottystyle\Laravel\SSO\Contracts;

interface Broker
{
    /**
     * Get the id of broker
     * 
     * @return mixed
     */
    public function getBrokerId();

    /**
     * Get the name of broker
     * 
     * @return string
     */
    public function getBrokerName();

    /**
     * Get the secret key of the broker
     * 
     * @return string
     */
    public function getSecret();
}