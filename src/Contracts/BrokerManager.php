<?php

namespace Dottystyle\Laravel\SSO\Contracts;

use Dottystyle\Laravel\SSO\UserInfo;
use Illuminate\Http\Request;

interface BrokerManager
{
    /**
     * Get the broker instance 
     * 
     * @return \Dottystyle\Laravel\SSO\Contracts\BrokerManager
     */
    public function getBroker();

    /**
     * Get the SSO server login URL to redirect the user.
     * 
     * @param string $continue The URL to redirect after successful login.
     * @return string
     */
    public function getLoginUrl(string $continue);

    /**
     * Receive the login response from the SSO server after a login attempt.
     * 
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function receiveLogin(Request $request);

    /**
     * Login the user given its user info from SSO server.
     * 
     * @param \Dottystyle\Laravel\SSO\UserInfo $info
     * @return mixed
     */
    public function login(UserInfo $info);

    /**
     * Get the logout URL of the server.
     * 
     * @param string $returnUrl
     * @return void
     */
    public function getLogoutUrl(string $returnUrl);

    /**
     * Get the SSO user.
     * 
     * @return mixed
     */
    public function getUser();

    /**
     * Set the SSO token.
     * 
     * @param string $token
     * @return void
     */
    public function setToken(string $token);

    /**
     * Get the name for the token.
     * 
     * @return string
     */
    public function getTokenName();
}