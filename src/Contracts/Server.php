<?php

namespace Dottystyle\Laravel\SSO\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

interface Server 
{
    /**
     * Get the user logged in.
     * 
     * @return \Dottystyle\Laravel\SSO\UserInfo 
     */
    public function getUserInfo();

    /**
     * Login the user.
     * 
     * @return \Dottystyle\Laravel\SSO\Contracts\Token 
     */
    public function login(Authenticatable $user);

    /**
     * Determines whether the login request is initiated by the broker.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Broker $broker
     * @param string $token
     * @param string $checksum
     * @return boolean
     */
    public function isValidBrokerLogin(Broker $broker, string $token, string $checksum);

    /**
     * Get the return url of a broker after a successful login.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Broker
     * @param string $url
     * @param array $params (optional)
     * @return string
     */
    public function getBrokerReturnUrl(Broker $broker, string $url, array $params = []);

    /**
     * Get the return url of a broker for a failed login.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Broker $broker
     * @param string $url
     * @return string
     */
    public function getFailedLoginBrokerReturnUrl(Broker $broker, string $url);

    /**
     * Get the token name.
     * 
     * @return string
     */
    public function getTokenName();

    /**
     * Set the token given its token id.
     * 
     * @param string $tokenId
     * @return void
     */
    public function setTokenById(string $tokenId);

    /**
     * Set the token given the token object.
     * 
     * @param string $tokenId
     * @return void
     */
    public function setToken(Token $token);

    /**
     * Get the token.
     * 
     * @return \Dottystyle\Laravel\SSO\Contracts\Token
     */
    public function getToken();

    /**
     * Invalidate the token.
     * 
     * @return void
     */
    public function invalidateToken();

}