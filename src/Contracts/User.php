<?php

namespace Dottystyle\Laravel\SSO\Contracts;

use Dottystyle\Laravel\SSO\UserInfo;

interface User
{
    /**
     * Fill the instance with details from the user info.
     * 
     * @param \Dottystyle\Laravel\SSO\UserInfo $info
     * @return void
     */
    public function setUserInfo(UserInfo $info);
}