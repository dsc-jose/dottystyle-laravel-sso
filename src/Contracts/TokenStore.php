<?php

namespace Dottystyle\Laravel\SSO\Contracts;

use Dottystyle\Laravel\SSO\Contracts\Token;

interface TokenStore
{
    /**
     * Create a token.
     * 
     * @param array $params
     * @return \Dottystyle\Laravel\SSO\Contracts\Token $token
     */
    public function create(array $params);

    /**
     * Put/save the given token.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Token $token
     * @return boolean
     */
    public function put(Token $token);

    /**
     * Get an SSO token by id.
     * 
     * @param string $id
     * @return \Dottystyle\Laravel\SSO\Contracts\Token
     */
    public function get(string $id);

    /**
     * Destroy/delete an SSO token by id.
     * 
     * @param string $id
     * @return boolean
     */
    public function destroy(string $id);
}