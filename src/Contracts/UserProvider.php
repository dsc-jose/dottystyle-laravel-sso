<?php

namespace Dottystyle\Laravel\SSO\Contracts;

interface UserProvider
{
    /**
     * Retrieve the user info by id.
     * 
     * @param mixed $userId
     * @return \Dottystyle\Laravel\SSO\UserInfo
     */
    public function retrieveById($userId);
}