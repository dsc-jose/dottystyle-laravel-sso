<?php

namespace Dottystyle\Laravel\SSO\Facades;

use Dottystyle\Laravel\SSO\Contracts\Server as ServerContract;
use Illuminate\Support\Facades\Facade;

class Server extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ServerContract::class;
    }
}