<?php

namespace Dottystyle\Laravel\SSO\Facades;

use Dottystyle\Laravel\SSO\Contracts\BrokerManager as BrokerManagerContract;
use Illuminate\Support\Facades\Facade;

class Broker extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return BrokerManagerContract::class;
    }
    
    /**
     * Logout the current SSO session.
     * 
     * @param string $returnUrl
     * @return void
     */
    public static function logout(string $returnUrl)
    {
        return self::$app['redirect']->to(
            self::$app[BrokerManagerContract::class]->getLogoutUrl($returnUrl)
        );
    }

    /**
     * Logout the current SSO session.
     * 
     * @param string $returnUrl
     * @return void
     */
    public static function logout(string $returnUrl)
    {
        return self::$app['redirect']->to(
            self::$app[BrokerManagerContract::class]->getLogoutUrl($returnUrl)
        );
    }
}