<?php

namespace Dottystyle\Laravel\SSO\Server\Middleware;

use Dottystyle\Laravel\SSO\Contracts\Server;
use Dottystyle\Laravel\SSO\Contracts\Token;
use Dottystyle\Laravel\SSO\Exceptions\AuthenticationException;
use Closure;

class AuthenticateToken
{
    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\Server 
     */
    protected $server;

    /**
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // Try to set the token on server by its id
            $token = $this->getRequestToken($request);

            $this->server->setTokenById($token);

            $this->handleToken($this->server->getToken(), $request);
        } catch (AuthenticationException $e) {
            $this->unauthenticated($request);
        } finally {
            return $next($request);
        }
    }

    /**
     * Handle the SSO token resolved from the request.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Token $token
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function handleToken(Token $token, $request)
    {
    }

    /**
     * Invoked when token is missing or not valid.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function unauthenticated($request)
    {
    }

    /**
     * Get the token from request.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getRequestToken($request)
    {
        return $request->cookies->get($this->server->getTokenName());
    }
}