<?php

namespace Dottystyle\Laravel\SSO\Server\Middleware;

use Dottystyle\Laravel\SSO\Contracts\Server;
use Dottystyle\Laravel\SSO\Contracts\Token;
use Dottystyle\Laravel\SSO\Concerns\AuthTokenMiddlewareHelpers;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\Cookie\QueueingFactory as CookieJar;
use Closure;

class StartServerSession extends AuthenticateToken
{
    use AuthTokenMiddlewareHelpers;

    /**
     * @param \Dottystyle\Laravel\SSO\Contracts\Server $server
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @param \Illuminate\Contracts\Cookie\QueueingFactory $cookies
     */
    public function __construct(
        Server $server, 
        AuthFactory $auth,
        CookieJar $cookies
    ) {
        parent::__construct($server);

        $this->auth = $auth;
        $this->cookies = $cookies;
    }

    /**
     * Handle the SSO token resolved from the request.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Token $token
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function handleToken(Token $token, $request)
    {
        $userId = $token->getUserId();

        $this->validateSameUser($userId);
        
        // Local session might have already expired so we are going to login the user manually using
        // the user id from token. 
        // Disable logging in if the request came directly from the broker server.
        if (!$this->auth->check()) {
            $this->auth->loginUsingId($userId);
        }
    }

    /**
     * Invoked when token is missing or not valid.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function unauthenticated($request)
    {
        $this->logoutUser($request);
    }

    /**
     * Get the token name used by the server.
     * 
     * @return string
     */
    protected function getTokenName()
    {
        return $this->server->getTokenName();
    }

    /**
     * Determine whether we should not login based on the current request route.
     * 
     * @param \Illuminate\Http\Request $request
     * @return boolean
     */
    protected function shoulNotLogin($request)
    {
        return in_array($request->route()->getName(), ['sso.user', 'sso.login', 'sso.logout']);
    }
}