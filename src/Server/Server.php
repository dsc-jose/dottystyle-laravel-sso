<?php

namespace Dottystyle\Laravel\SSO\Server;

use Dottystyle\Laravel\SSO\Contracts\Server as ServerContract;
use Dottystyle\Laravel\SSO\Contracts\Token;
use Dottystyle\Laravel\SSO\Contracts\TokenStore;
use Dottystyle\Laravel\SSO\Contracts\UserProvider;
use Dottystyle\Laravel\SSO\Contracts\Broker as BrokerContract;
use Dottystyle\Laravel\SSO\Support\Checksum;
use Dottystyle\Laravel\SSO\Exceptions\InvalidBrokerLoginRequestException;
use Dottystyle\Laravel\SSO\Exceptions\AuthenticationException;
use Dottystyle\Laravel\SSO\Exceptions\SSOException;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Str;
use Carbon\Carbon;

/**
 * Single sign-on server.
 *
 * The SSO server is responsible of managing users sessions which are available for brokers.
 * 
 * @see https://github.com/legalthings/sso
 */
class Server implements ServerContract
{
    use ServerBrokers;

    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\TokenStore
     */
    protected $tokens;

    /**
     * @var string
     */
    protected $tokenName;

    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\UserProvider
     */
    protected $users;

    /**
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * @var \Illuminate\Contracts\Cookie\QueueingFactory
     */
    protected $cookies;  

    /**
     * @var array 
     */
    protected $options = [];

    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\Token
     */
    protected $requestToken; 

    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\Token
     */
    protected $token;

    /**
     *
     * @param \Illumiante\Container\Container $app
     * @param string $tokenName
     * @param \Dottystyle\Laravel\SSO\Contracts\UserProvider $users
     * @param array $options (optional)
     */
    public function __construct(Container $app, string $tokenName, UserProvider $users, array $options = [])
    {
        $this->tokenName = $tokenName;
        $this->tokens = $app[TokenStore::class];
        $this->hasher = $app['hash'];
        $this->users = $users;
        $this->cookies = $app['cookie'];
        $this->options = array_merge($this->options, $options);
    }

    /**
     * Get the token name.
     * 
     * @return string
     */
    public function getTokenName()
    {
        return $this->tokenName;
    }

    /**
     * Set the token.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Token $token
     * @return void
     */
    public function setToken(Token $token)
    {
        $this->token = $token;
    }

    /**
     * Set the token given its token id.
     * 
     * @param string $tokenId
     * @return void
     */
    public function setTokenById(string $tokenId)
    {
        // Load the token and check its validity
        $token = $this->tokens->get($tokenId);

        if (!$token || $token->expired()) {
            throw new AuthenticationException();
        }

        $this->setToken($token);
    }

    /**
     * Get the token.
     * 
     * @return \Dottystyle\Laravel\SSO\Contracts\Token
     * 
     * @throws \Dottystyle\Laravel\SSO\Exceptions\AuthenticationException
     */
    public function getToken()
    {
        if (!$this->token) {
            throw new AuthenticationException();
        }
        
        return $this->token;
    }

    /**
     * Determines whether the login request is initiated by the broker.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Broker $broker
     * @param string $token
     * @param string $checksum
     * @return boolean
     */
    public function isValidBrokerLogin(BrokerContract $broker, string $token, string $checksum)
    {
        $checksumInput = Checksum::makeLoginRequestInput($token, $broker->getBrokerId(), $broker->getSecret());

        return $this->hasher->check($checksumInput, $checksum);
    }

    /**
     * Login the authenticatable user (create SSO token and set as cookie).
     * 
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @return \Dottystyle\Laravel\Contracts\Token
     */
    public function login(Authenticatable $user) 
    {
        $token = $this->generateToken($user->getAuthIdentifier());

        $this->setToken($token);

        $this->cookies->queue($this->tokenName, $token->getTokenId(), $token->remainingMinutes());

        return $token;
    }

    /**
     * Generate a global SSO token that will be shared among brokers.
     * 
     * @param mixed $userId
     * @return \Dottystyle\Laravel\SSO\Contracts\Token
     */
    protected function generateToken($userId) 
    {
        $token = $this->tokens->create(['user_id' => $userId]);
        $this->tokens->put($token);

        return $token;
    }

    /**
     * Get the return url of a broker after a successful login.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Broker
     * @param string $url
     * @param array $params (optional)
     * @return string
     */
    public function getBrokerReturnUrl(BrokerContract $broker, string $url, array $params = [])
    {
        $token = $this->getToken();
        $userId = $token->getUserId();

        $checksumInput = Checksum::makeLoginSuccessInput(
            $broker->getBrokerId(), 
            $broker->getSecret(), 
            $token->getTokenId(), 
            $userId
        );

        $expiresAt = Carbon::now()->addMinutes($token->remainingMinutes())->getTimestamp();

        $params = array_merge([
            'user_id' => $userId,
            'sso_token' => $token->getTokenId(),
            'broker' => $broker->getBrokerId(),
            'checksum' => $this->hasher->make($checksumInput),
            'expires_at' => $expiresAt,
            'login_success' => 1
        ], $params);

        return $this->appendQueryParams($url, $params);
    }

    /**
     * Get the return url of a broker for a failed login.
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Broker $broker
     * @param string $url
     * @return string
     */
    public function getFailedLoginBrokerReturnUrl(BrokerContract $broker, string $url)
    {
        $checksumInput = Checksum::makeLoginFailedInput($broker->getBrokerId(), $broker->getSecret());
        $checksum = $this->hasher->make($checksumInput);

        $params = compact('checksum') + ['login_success' => 0];

        return $this->appendQueryParams($url, $params);
    }

    /**
     * Append query parameters to the given url.
     * 
     * @param string $url
     * @param array $params
     * @return string
     */
    protected function appendQueryParams(string $url, array $params) 
    {
        // Preserve any query parameters on the query
        $query = parse_url($url, PHP_URL_QUERY);
        $queryString = http_build_query($params);
        $query = ($query ? '&' : '?').$queryString;

        return $url.$query;
    }

    /**
     * Get the user from the SSO token.
     * 
     * @return \Dottystyle\Laravel\SSO\UserInfo
     *
     * @throws \Dottystyle\Laravel\SSO\Exceptions\AuthenticationException
     */
    public function getUserInfo() 
    {
        $user = $this->users->retrieveById($this->getToken()->getUserId());

        if (!$user) {
            throw new AuthenticationException();
        }

        return $user;
    }

    /**
     * Invalidate the current token.
     * 
     * @return void
     */
    public function invalidateToken()
    {
        // Do nothing if token is unavailable
        if ($this->token) {
            $this->tokens->destroy($this->getToken()->getTokenId());
        }
    }
}