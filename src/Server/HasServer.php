<?php

namespace Dottystyle\Laravel\SSO\Server;

use Dottystyle\Laravel\SSO\Contracts\Server;

trait HasServer
{
    /**
     * Set the SSO server instance
     * 
     * @param \Dottystyle\Laravel\SSO\Contracts\Server
     * @return void
     */
    protected function getServer()
    {
        return app()->make(Server::class);
    }
}
