<?php

namespace Dottystyle\Laravel\SSO\Server;

use Dottystyle\Laravel\SSO\Exceptions\AuthenticationException as SSOAuthenticationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServerController extends Controller
{
    use HasServer;

    /**
     * Get the logged in SSO user.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response $response
     */
    public function getUser(Request $request) 
    {
        try {
            $user = $this->getServer()->getUserInfo();

            return response()->json($user->toArray());
        } catch (SSOAuthenticationException $e) {
            return response()->json([], 401);
        }
    }

    /**
     * Logout the SSO user (delete token and logout session)
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response 
     */
    public function logout(Request $request)
    {
        // Return to our home if no return url was sent by the broker
        $returnUrl = $request->input('return_url', route('home'));

        // Logout using the authentication guard to trigger the server token invalidation
        Auth::logout();

        return redirect($returnUrl);
    }
}