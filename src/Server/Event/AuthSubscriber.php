<?php

namespace Dottystyle\Laravel\SSO\Server\Event;

use Dottystyle\Laravel\SSO\Contracts\Server;

class AuthSubscriber 
{
    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\Server
     */
    protected $server;

    /**
     * @param \Dottystyle\Laravel\SSO\Contracts\Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function subscribe($events)
    {
        $class = get_class($this);

        $events->listen('Illuminate\Auth\Events\Login', "$class@onLogin");
        $events->listen('Illuminate\Auth\Events\Logout', "$class@onLogout");
    }

    /**
     * Handle user login events.
     * 
     * @param mixed $event
     * @return void
     */
    public function onLogin($event)
    {
        // Issue a new token
        $this->server->login($event->user);
    }

    /**
     * Handle user logout events.
     * 
     * @param mixed $event
     * @return void
     */
    public function onLogout($event)
    {
        $this->server->invalidateToken();
    }
}
