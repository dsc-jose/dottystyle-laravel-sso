<?php

namespace Dottystyle\Laravel\SSO\Server\Token;

use Dottystyle\Laravel\SSO\Server\Token\Model as Token;
use Dottystyle\Laravel\SSO\Contracts\TokenStore as StoreContract;
use Dottystyle\Laravel\SSO\Contracts\Token as TokenContract;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Store implements StoreContract
{
    /**
     * @var int
     */
    protected $lifetime;

    /**
     * @param int $lifetime The lifetime of the token in minutes.
     */
    public function __construct(int $lifetime)
    {
        $this->lifetime = $lifetime;
    }

    /**
     * Create a token.
     * 
     * @param array $params 
     * @param string $params['auth_id']
     * @return \Dottystyle\Laravel\SSO\Contracts\Token $token
     */
    public function create(array $params)
    {
        $token = new Token();
    
        do {
            // Try generating an id until we have a unique one
            $id = Str::random(60);
        } while (Token::find($id));

        $token->id = $id;
        $token->user_id = $params['user_id'];
        $token->expired_at = Carbon::now()->addMinutes($this->lifetime);

        return $token;
    }

    /**
     * Save the given token.
     * 
     * @return \Dottystyle\Laravel\SSO\Contracts\Token
     */
    public function put(TokenContract $token)
    {
        // Save the token here since the tokens are already given a unique id on create method. 
        $token->save();

        return true;
    }

    /**
     * Get an SSO token by id.
     * 
     * @param string $id
     * @return \Dottystyle\Laravel\SSO\Contracts\Token
     */
    public function get(string $id)
    {
        $token = Token::find($id);

        if ($token && !$token->expired()) {
            return $token;
        }
    
        return null;
    }

    /**
     * Destroy/delete an SSO token by its id.
     * 
     * @param string $id
     * @return boolean
     */
    public function destroy(string $id)
    {
        return (bool) Token::destroy($id);
    }
}