<?php

namespace Dottystyle\Laravel\SSO\Server;

use Dottystyle\Laravel\SSO\Contracts\TokenStore as TokenStoreContract;
use Dottystyle\Laravel\SSO\Server\Token\Store as TokenStore;
use Dottystyle\Laravel\SSO\Contracts\Server as ServerContract;
use Dottystyle\Laravel\SSO\RegistersAuthGuardUserProvider;
use Illuminate\Support\ServiceProvider;

class SSOServiceProvider extends ServiceProvider
{
    use RegistersAuthGuardUserProvider;

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([ __DIR__.'/config.php' => config_path('sso.php') ]);
        $this->mergeConfigFrom(__DIR__.'/config.php', 'sso');

        // Register routes
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Register migrations
        $this->loadMigrationsFrom(__DIR__.'/migrations');

        // Register our views
        $this->loadViewsFrom(dirname(__DIR__).'/resources/views/server', 'sso.server');

        $this->registerMiddleware();

        $this->registerEventSubscriber();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAuthGuardUserProvider();
        $this->registerServer();
        $this->registerTokenStore();
    }

    /**
     * Register the server.
     * 
     * @return void
     */
    protected function registerServer()
    {
        $this->app->singleton(ServerContract::class, function ($app) {
            // Get the user provider from authentication guard.
            $users = $this->getAuthGuardUserProvider();
            $server = new Server($app, $app['config']['sso.token'], $users);

            // Load brokers from a JSON file
            $server->setBrokersFromJsonFile($app['config']['sso.brokers_json']);

            return $server;
        });
    }

    /**
     * Register the token class.
     * 
     * @return void
     */
    protected function registerTokenStore()
    {
        $this->app->singleton(TokenStoreContract::class, function ($app) {
            // Create first the server instance then return the token store
            return new TokenStore($app['config']['sso.token_lifetime']);
        });
    }

    /**
     * Register our middleware.
     * 
     * @return void
     */
    protected function registerMiddleware()
    {
        $this->app['router']->aliasMiddleware('sso.server', Middleware\StartServerSession::class);
        $this->app['router']->aliasMiddleware('sso.auth_token', Middleware\AuthenticateToken::class);
    }

    /**
     * Register the subscriber class.
     *
     * @return void
     */
    protected function registerEventSubscriber()
    {
        $this->app['events']->subscribe(Event\AuthSubscriber::class);
    }

    /**
     * @return string
     */
    public function provides()
    {
        return [ServerContract::class];
    }
}