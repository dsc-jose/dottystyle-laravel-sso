<?php

return [
    /**
     * @var array
     */
    'brokers_json' => env('SSO_BROKERS_JSON', storage_path('sso/brokers.json')),
    /**
     * @var int The minimum length for the broker's secret
     */
    'broker_secret_min' => 32,
    /**
     * @var int 
     */
    'token_lifetime' => env('SSO_TOKEN_LIFETIME', 60 * 24 * 60),
    /**
     * @var string
     */
    'token' => env('SSO_TOKEN', 'sso_token'),
    /**
     * 
     */
    'users' => [],
    /**
     * @var boolean
     * 
     * Set to true to exempt token from being encrypted. 
     */
    'cookie_noencrypt' => env('SSO_TOKEN_COOKIE_ENCRYPT', true),
    /**
     * @var string 
     * 
     * Adds exception to the SSO token when encrypting cookies.
     * Put the middleware class that encrypts the cookies here. 
     */
    'cookie_encrypt_middleware' => ''
];