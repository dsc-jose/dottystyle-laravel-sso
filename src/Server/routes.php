<?php

Route::prefix('sso')
    ->group(function () {
        $controller = Dottystyle\Laravel\SSO\Server\ServerController::class;

        Route::get('user', "$controller@getUser")->name('sso.user')->middleware('sso.auth_token');

        Route::get('logout', "$controller@logout")->name('sso.logout')->middleware('web', 'sso.server');

    });