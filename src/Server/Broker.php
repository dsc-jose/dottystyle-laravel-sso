<?php

namespace Dottystyle\Laravel\SSO\Server;

use InvalidArgumentException;
use Dottystyle\Laravel\SSO\Contracts\Broker as BrokerContract;

class Broker implements BrokerContract
{
    const DEFAULT_SECRET_MIN_LENGTH = 32;

    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var int
     */
    private static $secretMinLength = self::DEFAULT_SECRET_MIN_LENGTH;

    /**
     * @param mixed $id
     * @param string $name
     * @param string $secret
     */
    public function __construct($id, string $name, string $secret)
    {
        // Prevent empty/unset, short secret keys
        if (empty($secret) || strlen($secret) < self::$secretMinLength) {
            throw new InvalidArgumentException("Broker secret must not be not shorter than ".self::getSecretMinLength());
        }
        
        $this->id = $id;
        $this->name = $name;
        $this->secret = $secret;
    }

    /**
     * Set the minimum length for broker secret.
     * 
     * @param int $length
     * @return void
     */
    public static function setSecretMinLength(int $length)
    {
        self::$secretMinLength = max($length, self::DEFAULT_SECRET_MIN_LENGTH);
    }

    /**
     * Get the minimum length for broker secret
     * 
     * @return int
     */
    public static function getSecretMinLength()
    {
        return self::$secretMinLength;
    }

    /**
     * Get the id of broker
     * 
     * @return mixed
     */
    public function getBrokerId()
    {
        return $this->id;
    }

    /**
     * Get the name of broker
     * 
     * @return string
     */
    public function getBrokerName()
    {
        return $this->name;
    }

    /**
     * Get the secret key of the broker
     * 
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }
}