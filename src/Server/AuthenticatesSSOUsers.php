<?php

namespace Dottystyle\Laravel\SSO\Server;

use Dottystyle\Laravel\SSO\Support\Request as RequestUtils;
use Dottystyle\Laravel\SSO\Exceptions\SSOException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait AuthenticatesSSOUsers
{
    use HasServer;

    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\Broker
     */
    protected $broker;

    /**
     * Show the login form.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showSSOLoginForm(Request $request)
    {
        try {
            if (!$this->isBrokerLogin($request)) {
                throw new SSOException();
            } 

            $authenticated = Auth::check();

            if (RequestUtils::acceptsImage($request) || $authenticated) {
                return $this->returnToBroker($request, $authenticated);
            } 
            
            // Pass all query parameters
            $action = $this->getLoginAuthUrl($request);

            return view($this->getLoginFormView(), ['action' => $action]);
        } catch (SSOException $e) {
            return view('sso.server::login-request-error');
        }
    }

    /**
     * Get the URL to do authentication.
     * 
     * @return \Illuminate\Http\Response
     */
    protected function getLoginAuthUrl(Request $request) 
    {
        return route('login', $request->query());
    }

    /**
     * Get the view for the login form
     * 
     * @return string
     */
    protected function getLoginFormView()
    {
        return property_exists($this, 'loginView') ? $this->loginView : 'auth.login';
    }

    /**
     * Get additional controller methods that should be excluded from the guest middleware.
     * 
     * @return array
     */
    protected function getExcludedMethodsToGuestMiddleware()
    {
        return ['showSSOLoginForm'];
    }

    /**
     * Determine whether the login has been requested by the broker.
     * 
     * @param \Illuminate\Http\Request $request
     * @return boolean
     */
    protected function isBrokerLogin(Request $request) {
        $broker = $this->getServer()->getBroker($request->input('broker'));

        if (!$broker || !$request->input('return_url')) {
            return false;
        }

        $this->broker = $broker;
        $token = $request->input('token', '');
        $checksum = $request->input('checksum', '');

        return $this->getServer()->isValidBrokerLogin($broker, $token, $checksum);
    }

    /**
     * Create a redirect response to the broker's redirect url.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param bool $authenticated
     * @param bool $silent
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function returnToBroker(Request $request, bool $authenticated)
    {
        $returnUrl = $request->input('return_url');

        if ($authenticated) {
            $url = $this->getServer()->getBrokerReturnUrl($this->broker, $returnUrl);
        } else {
            $url = $this->getServer()->getFailedLoginBrokerReturnUrl($this->broker, $returnUrl);
        }

        return redirect($url);
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request)
    {
        if ($this->isBrokerLogin($request)) {
            // At this point we have already a token so we just return back to the broker.
            return $this->returnToBroker($request, true);
        }
    }
}