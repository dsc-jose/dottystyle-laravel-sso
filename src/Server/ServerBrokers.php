<?php

namespace Dottystyle\Laravel\SSO\Server;

use Illuminate\Http\Request;

trait ServerBrokers 
{
    /**
     * @var array
     */
    protected $brokers = [];

    /**
     * Set the brokers
     * 
     * @param \Dottystyle\Laravel\SSO\Server\Broker[] $brokers
     * @return void
     */
    public function setBrokers(array $brokers)
    {
        $this->brokers = $brokers;
    }

    /**
     * Get broker by its id
     * 
     * @param string $id
     * @return \Dottystyle\Laravel\SSO\Server\Broker
     */
    public function getBroker($id)
    {
        return $this->brokers[$id] ?? null;
    }

    /**
     * Get the brokers
     * 
     * @return array
     */
    public function getBrokers()
    {
        return $this->brokers;
    }
    
    /**
     * Load brokers from a JSON file.
     * 
     * @param string $path
     * @return int
     */
    public function setBrokersFromJsonFile(string $path)
    {
        $count = 0;

        if (!file_exists($path) || !is_readable($path)) {
            return $count;
        }
        
        $brokers = @json_decode(file_get_contents($path), true);
        
        if (is_array($brokers) && $brokers) {
            foreach ($brokers as $broker) {
                $broker = new Broker($broker['id'], $broker['name'], $broker['secret']);
                $this->brokers[$broker->getBrokerId()] = $broker;
                $count++;
            }
        }
        
        return $count;
    }
}