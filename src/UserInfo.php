<?php

namespace Dottystyle\Laravel\SSO;

use Illuminate\Contracts\Support\Arrayable;

class UserInfo implements Arrayable 
{
    /**
     * @var array
     */
    protected $info;

    /**
     * @param array $info
     */
    public function __construct(array $info) 
    {
        $this->info = $info;
    }

    /**
     * Get the id of the user.
     * 
     * @return mixed
     */
    public function getId()
    {
        return $this->info['id'];
    }

    /**
     * Convert the instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->info;
    }
}