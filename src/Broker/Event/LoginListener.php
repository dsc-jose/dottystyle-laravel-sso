<?php

namespace Dottystyle\Laravel\SSO\Broker\Event;

use Dottystyle\Laravel\SSO\Contracts\BrokerUserProvider;
use Illuminate\Contracts\Auth\Guard;

class LoginListener
{
    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\BrokerUserProvider
     */
    protected $users;

    /**
     * @var Illuminate\Contracts\Auth\Guard
     */
    protected $authGuard;

    /**
     * @param \Dottystyle\Laravel\SSO\Contracts\BrokerUserProvider $users
     */
    public function __construct(BrokerUserProvider $users, Guard $guard)
    {
        $this->users = $users;
        $this->authGuard = $guard;
    }

    /**
     * Handle the login event.
     * 
     * @param \Dottystyle\Laravel\SSO\Broker\Event\Login $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $this->users->retrieveByUserInfo($event->getUserInfo());

        // Then login using the authentication guard
        $this->authGuard->login($user);
    }
}