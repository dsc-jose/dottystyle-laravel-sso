<?php

namespace Dottystyle\Laravel\SSO\Broker\Event;

use Dottystyle\Laravel\SSO\UserInfo;

class Login 
{
    /**
     * @param \Dottystyle\Laravel\SSO\UserInfo $user
     */
    protected $user;

    /**
     * @param \Dottystyle\Laravel\SSO\UserInfo $user
     */
    public function __construct(UserInfo $user)
    {
        $this->user = $user;
    }

    /**
     * Get the info of the logged in user.
     * 
     * @return \Dottystyle\Laravel\SSO\UserInfo
     */
    public function getUserInfo()
    {
        return $this->user;
    }
}