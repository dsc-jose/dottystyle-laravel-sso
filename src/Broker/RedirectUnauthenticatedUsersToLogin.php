<?php

namespace Dottystyle\Laravel\SSO\Broker;

use Illuminate\Auth\AuthenticationException;

/**
 * Trait that can be used on exception handler to redirect the user to SSO login page/route.
 */
trait RedirectUnauthenticatedUsersToLogin
{
    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
            ? response()->json(['message' => $exception->getMessage()], 401)
            : redirect()->guest($this->getSSOLoginRoute($request));
    }

    /**
     * Get the route to SSO login.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getSSOLoginRoute($request)
    {
        return route('sso.login', [ 'continue' => $request->fullUrl() ]);
    }
}