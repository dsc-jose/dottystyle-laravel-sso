<?php

namespace Dottystyle\Laravel\SSO\Broker;

use Dottystyle\Laravel\SSO\Contracts\Broker as BrokerContract;

class Broker implements BrokerContract
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @param mixed $id
     * @param string $name
     * @param string $secret
     */
    public function __construct($id, string $name, string $secret)
    {
        $this->id = $id;
        $this->name = $name;
        $this->secret = $secret;
    }

    /**
     * Get the id of broker
     * 
     * @return mixed
     */
    public function getBrokerId()
    {
        return $this->id;
    }

    /**
     * Get the name of broker
     * 
     * @return string
     */
    public function getBrokerName()
    {
        return $this->name;
    }

    /**
     * Get the secret key of the broker
     * 
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }
}