<?php

return [
   'broker' => [
        /**
         * @var string 
         */
        'id' => env('SSO_BROKER_ID'),
        /**
         * @var string 
         */
        'name' => env('SSO_BROKER_NAME'),
        /**
         * @var string
         */
        'secret' => env('SSO_BROKER_SECRET'),
        /**
         * @var string The name of the SSO token
         */
        'token' => env('SSO_BROKER_TOKEN'),
        /**
         * @var array
         */
        'users' => [
            'model' => ''
        ],
        /**
         * @var string Where to redirect after logging out
         */
        'logout_redirect' => '/'
   ],
   /**
    * @var string
    */
   'server_url' => env('SSO_SERVER_URL')
];