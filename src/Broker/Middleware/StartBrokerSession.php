<?php

namespace Dottystyle\Laravel\SSO\Broker\Middleware;

use Dottystyle\Laravel\SSO\Contracts\BrokerManager;
use Dottystyle\Laravel\SSO\UserInfo;
use Dottystyle\Laravel\SSO\Exceptions\GetUserInfoException;
use Dottystyle\Laravel\SSO\Exceptions\AuthenticationException;
use Dottystyle\Laravel\SSO\Concerns\ChecksAuthAndToken;
use Illuminate\Contracts\Cookie\QueueingFactory as CookieJar;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Http\Request;
use Closure;

class StartBrokerSession
{
    use ChecksAuthAndToken;

    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\BrokerManager
     */
    protected $manager;

    /**
     * @param \Dottystyle\Laravel\SSO\Contracts\BrokerManager $manager
     * @param \Illuminate\Contracts\Auth\Factory $auth
     * @param \Illuminate\Contracts\Cookie\QueueingFactory $cookies
     */
    public function __construct(BrokerManager $manager, AuthFactory $auth, CookieJar $cookies)
    {
        $this->manager = $manager;
        $this->auth = $auth;
        $this->cookies = $cookies;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        try {
            $token = $this->getRequestToken($request);
            // Logout the user if we don't have any token
            if (!$token) {
                throw new AuthenticationException();
            }

            $this->manager->setToken($token);
            $userInfo = $this->manager->getUser();

            $this->validateSameUser($userInfo->getId());

            if (!$this->auth->check()) {
                $this->manager->login($userInfo);
            }
        } catch (GetUserInfoException $e) {
            // We could not get the user info from our token for some reason.
            // TODO
            // Is this the correct action?
            $this->logoutUser($request);
        } catch (AuthenticationException $e) {
            // Force logout even if the user is logged in since it is likely we have an invalid token
            $this->logoutUser($request);
        } finally {
            return $next($request);
        }
    }
    
    /**
     * Log the user on the application using the retrieved user info.
     * Does nothing if user is already logged in.
     * 
     * @param \Dottystyle\Laravel\SSO\UserInfo $userInfo
     * @return void
     */
    protected function loginUser(UserInfo $userInfo)
    {   
        if (!$this->auth->check()) {
            $this->manager->login($userInfo);
        }
    }

    /**
     * Get the token name used by the broker manager.
     * 
     * @return string
     */
    protected function getTokenName()
    {
        return $this->manager->getTokenName();
    }
}