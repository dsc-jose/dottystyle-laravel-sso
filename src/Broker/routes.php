<?php

Route::prefix('sso')
    ->namespace('Dottystyle\Laravel\SSO\Broker')
    ->middleware(['web', 'sso.broker'])
    ->group(function() {

        Route::get('login', 'BrokerController@login')->name('sso.login');
        Route::get('login_cb', 'BrokerController@receiveLogin')->name('sso.receive_login');
        Route::get('logout', 'BrokerController@logout')->name('sso.logout');

    });