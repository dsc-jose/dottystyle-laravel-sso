<?php

namespace Dottystyle\Laravel\SSO\Broker;

use Dottystyle\Laravel\SSO\Contracts\BrokerUserProvider;
use Dottystyle\Laravel\SSO\UserInfo;

class EloquentUserProvider implements BrokerUserProvider
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @param string $model
     */
    public function __construct(string $model)
    {
        $this->model = $model;
    }

    /**
     * Save the user from SSO to our local user repository.
     * 
     * @param \Dottystyle\Laravel\SSO\UserInfo $userInfo
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function retrieveByUserInfo(UserInfo $userInfo)
    {
        $modelClass = $this->model;

        $user = $modelClass::find($userInfo->getId());
        // Create a new record if not yet existing
        $user or ($user = new $modelClass());

        $user->setUserInfo($userInfo);  
        // Save new user or update existing 
        $user->save();

        return $user;
    }
}