<?php

namespace Dottystyle\Laravel\SSO\Broker;

use Dottystyle\Laravel\SSO\Contracts\BrokerManager as BrokerManagerContract;
use Dottystyle\Laravel\SSO\Support\Request as RequestUtils;
use Dottystyle\Laravel\SSO\Exceptions\SSOException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class BrokerController extends Controller
{
    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\BrokerManager
     */
    protected $brokerManager;

    /**
     * @param \Dottystyle\Laravel\SSO\Broker\Broker
     */
    public function __construct(BrokerManagerContract $brokerManager)
    {
        $this->brokerManager = $brokerManager;
    }

    /**
     * Redirect the user to the SSO server login page
     * 
     * @param \Illuminate\Http\Request $request 
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $continue = $request->query('continue') ?: $this->getLoginSuccessRedirectUrl();  
        $url = $this->brokerManager->getLoginUrl($continue);

        return redirect($url);
    }

    /**
     * Receives and verifies login response from the SSO server.
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function receiveLogin(Request $request)
    {
        $acceptsImage = RequestUtils::acceptsImage($request);

        try {
            $response = $this->brokerManager->receiveLogin($request);

            // Return the proper response based on the request details.
            if ($response instanceof Response) {
                return $response;
            } 
            
            if ($acceptsImage) {
                return $this->respondWithImage($request, true);
            } 
            
            if ($continue = $request->query('continue')) {
                return redirect($continue);
            }

            return redirect($this->getLoginSuccessRedirectUrl());
        } catch (SSOException $e) {
            if ($acceptsImage) {
                return $this->respondWithImage($request, false);
            }

            return view('sso.broker::login-receive-error');
        }   
    }

    /**
     * Respond with a 1x1 pixel image.
     * 
     * @param \Illuminate\Http\Request $request
     * @param bool $loggedIn
     * @return \Illuminate\Http\Response
     */
    public function respondWithImage(Request $request, bool $loggedIn)
    {
        // Do not cache the result and set a cookie to check whether we have logged in or not, good for only a minute.
        return response(base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII='))
            ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
            ->header('Content-Type', 'image/png')
            // Let the script access the flag
            ->cookie('sso_autologin_success', $loggedIn ? '1' : '0', 1, null, null, false, false);
    }

    /**
     * Logout the user (deletes token thereby invalidating current session).
     * 
     * @return void 
     */
    public function logout()
    {
        $returnUrl = route(config('sso.broker.logout_redirect'));

        return redirect($this->brokerManager->getLogoutUrl($returnUrl));
    }

    /**
     * Get the redirect url after a successful login.
     * 
     * @return string
     */
    protected function getLoginSuccessRedirectUrl()
    {
        $route = config('sso.broker.login_success_redirect');
        
        return $route ? route($route) : url('/');
    }
}