<?php

namespace Dottystyle\Laravel\SSO\Broker;

use Dottystyle\Laravel\SSO\Contracts\BrokerManager as BrokerManagerContract;
use Dottystyle\Laravel\SSO\Contracts\BrokerUserProvider;
use Dottystyle\Laravel\SSO\Broker\Middleware\StartBrokerSession;
use Dottystyle\Laravel\SSO\Broker\Event\Login;
use Dottystyle\Laravel\SSO\Broker\Event\LoginListener;
use Illuminate\Support\ServiceProvider;

class SSOServiceProvider extends ServiceProvider
{
    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([ __DIR__.'/config.php' => config_path('sso.php') ], 'config');
        $this->publishes([ __DIR__.'/' ]);

        $this->mergeConfigFrom(__DIR__.'/config.php', 'sso');

        // Register routes
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Register our views
        $this->loadViewsFrom(dirname(__DIR__).'/resources/views/broker', 'sso.broker');

        $this->registerEventListeners();

        $this->registerViewComposer();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('sso.broker_manager', function ($app) {
            $config = $app['config'];
            
            $manager = new BrokerManager(
                $app, 
                $config['sso.server_url'], 
                $config['sso.token'
            ]);
            
            return $manager;
        });

        $this->app->alias('sso.broker_manager', BrokerManagerContract::class);

        $this->app->singleton('sso.broker', function ($app) {
            $config = $app['config'];

            return new Broker(
                $config['sso.broker.id'], 
                $config['sso.broker.name'], 
                $config['sso.broker.secret']
            );
        });

        $this->registerUserProvider();

        $this->registerMiddleware();
    }

    /**
     * Register the user provider
     * 
     * @return void
     */
    protected function registerUserProvider()
    {
        // Register resolver/provider for broker users
        $this->app->singleton(BrokerUserProvider::class, function ($app) {
            return new EloquentUserProvider($app['config']['sso.broker.users.model']);
        });
    }

    /**
     * Register the broker middleware.
     * 
     * @return void
     */
    protected function registerMiddleware()
    {
        $this->app->bind(StartBrokerSession::class, function ($app) {
            return new StartBrokerSession(
                $app['sso.broker_manager'],
                $app['auth'],
                $app['cookie']
            );
        });

        $this->app['router']->aliasMiddleware('sso.broker', StartBrokerSession::class);
    }

    /**
     * Register event listeners.
     * 
     * @return void
     */
    protected function registerEventListeners()
    {
        $this->app['events']->listen(Login::class, LoginListener::class);
    }

    /**
     * Register view composer.
     * 
     * @return void
     */
    protected function registerViewComposer()
    {
        $this->app['view']->composer('sso.broker::autologin', function($view) {
            $view->with([
                'src' => $this->app['sso.broker_manager']->getLoginUrl(url('/')),
                'cookie_var' => 'sso_autologin_success'
            ]);
        });
    }

    /**
     * 
     * @return array
     */
    public function provides()
    {
        return ['sso.broker_manager', BrokerManagerContract::class];
    }
}