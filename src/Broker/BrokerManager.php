<?php

namespace Dottystyle\Laravel\SSO\Broker;

use Dottystyle\Laravel\SSO\Contracts\BrokerManager as BrokerManagerContract;
use Dottystyle\Laravel\SSO\Contracts\BrokerUserMapper as UserMapper;
use Dottystyle\Laravel\SSO\UserInfo;
use Dottystyle\Laravel\SSO\Support\Checksum;
use Dottystyle\Laravel\SSO\SendsHttpRequests;
use Dottystyle\Laravel\SSO\Exceptions\SSOException;
use Dottystyle\Laravel\SSO\Exceptions\GetUserInfoException;
use Dottystyle\Laravel\SSO\Exceptions\MissingTokenException;
use Dottystyle\Laravel\SSO\Exceptions\AuthenticationException;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use GuzzleHttp\Exception\TransferException;

class BrokerManager implements BrokerManagerContract
{
    use SendsHttpRequests;

    /**
     * @var \Illuminate\Container\Container
     */
    protected $app;

    /**
     * @var \Dottystyle\Laravel\SSO\Contracts\Broker $broker
     */
    protected $broker;

    /**
     * @var string
     */
    protected $serverUrl;

    /**
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * @var \Illuminate\Contracts\Cookie\QueueingFactory
     */
    protected $cookies;  
    
    /**
     * @var string 
     */
    protected $tokenName;

    /**
     * @var string The SSO token received from the server (or in cookie)
     */
    protected $token;

    /**
     * 
     * @param \Illuminate\Container\Container $app
     * @param string $url
     * @param string $tokenName
     * @param \Dottystyle\Laravel\SSO\Contracts\BrokerUserMapper
     */
    public function __construct(Container $app, string $url, string $tokenName) 
    {
        $this->app = $app;
        $this->serverUrl = rtrim($url, '/');
        $this->tokenName = $tokenName;
        $this->broker = $app['sso.broker'];
        $this->hasher = $app['hash'];
        $this->cookies = $app['cookie'];
    }

    /**
     * Get the broker instance.
     * 
     * @return \Dottystyle\Laravel\SSO\Contracts\Broker
     */
    public function getBroker() 
    {
        return $this->broker;
    }

    /**
     * Set the SSO token.
     * 
     * @param string $token
     * @return void
     */
    public function setToken(string $token) 
    {
        $this->token = $token;
    }

    /**
     * Get the name for the token.
     * 
     * @return string
     */
    public function getTokenName()
    {
        return $this->tokenName;
    }

    /**
     * Ensures token is set. 
     * Throws an MissingTokenException when token is not set.
     * 
     * @throws \Dottystyle\Laravel\SSO\Exceptions\MissingTokenException
     */
    protected function ensureToken() 
    {
        if (!$this->token) {
            throw new MissingTokenException();
        }
    }

    /**
     * Create and send an HTTP request to server.
     * 
     * @param string $method
     * @param string $url
     * @param array $options (optional)
     * @return \GuzzleHttp\Message\ResponseInterface
     */
    public function sendServerRequest(string $method, string $url, array $options = []) {
        // Add the SSO token for every request sent to the server.
        if ($this->token) {
            $options['cookies'] = $options['cookies'] ?? []; 
            $options['cookies'][$this->tokenName] = $this->token;
        }

        $request = $this->getHttpClient()->createRequest($method, $url, $options);

        return $this->getHttpClient()->send($request);
    }

    /**
     * Get the options when creating http client instance.
     * 
     * @return array
     */
    protected function getHttpClientOptions()
    {
        return [
            'headers' => ['Content-Type' => 'application/json']
        ];
    }

    /**
     * Get URL to attach session at SSO server.
     *
     * @param string $continue
     * @return string
     */
    public function getLoginUrl(string $continue)
    {
        $token = Str::random(32);
        $checksumInput = Checksum::makeLoginRequestInput(
            $token, $this->broker->getBrokerId(), $this->broker->getSecret()
        );
        $returnUrl = $this->app['url']->route('sso.receive_login', compact('continue'));
        
        $data = [
            'broker' => $this->broker->getBrokerId(),
            'token' => $token,
            'checksum' => $this->hasher->make($checksumInput),
            'return_url' => $returnUrl
        ];
        
        return $this->serverUrl."/sso/login?".http_build_query($data);
    }

    /**
     * Login the user given its user info from SSO server.
     * 
     * @param \Dottystyle\Laravel\SSO\UserInfo $info
     * @return mixed
     */
    public function login(UserInfo $user)
    {
        $this->app['events']->fire(new Event\Login($user));
    }

    /**
     * Receive the login response from the SSO server after a login attempt.
     * 
     * @param \Illuminate\Http\Request $request
     * @return mixed
     * 
     * @throws \Dottystyle\Laravel\SSO\SSOException
     */
    public function receiveLogin(Request $request)
    {
        if (!$this->isValidServerLoginResponse($request)) {
            throw new SSOException("Invalid SSO server login received");
        }

        $this->setToken($token = $request->input('sso_token'));
        
        // Fetch the user data from SSO using the freshly received token
        $user = $this->getUser();

        // Create and set the cookie for the token.
        // The expiration (timestamp) of the cookie is sent together with the token to 
        // compute the remaining minutes of the token relative to broker's time.
        $expiresAt = Carbon::createFromTimestamp($request->input('expires_at'));
        $now = Carbon::now();
        $remainingMinutes = max($expiresAt->diffInMinutes($now), 0);
        
        $cookie = $this->cookies->make($this->tokenName, $token, $remainingMinutes);
        $this->cookies->queue($cookie);
        
        $this->login($user);
    }

    /**
     * Get the SSO user.
     * 
     * @return \Dottystyle\Laravel\SSO\UserInfo
     */
    public function getUser()
    {
        try {
            $this->ensureToken();
            
            $response = $this->sendServerRequest('GET', $this->serverUrl.'/sso/user');
            
            if ($response->getStatusCode() !== 200) {
                throw new GetUserInfoException();
            }

            return new UserInfo($response->json());
        } catch (TransferException $e) {
            throw new GetUserInfoException();
        }
    }

    /**
     * Validate the received login response from SSO server.
     * 
     * @param \Illuminate\Http\Request $request
     * @return boolean
     */
    protected function isValidServerLoginResponse(Request $request) {
        if (!$request->has(['broker', 'user_id', 'sso_token', 'checksum']) || $request->input('broker') !== $this->broker->getBrokerId()) {
            return false;
        }

        $checksumInput = Checksum::makeLoginSuccessInput(
            $this->broker->getBrokerId(),
            $this->broker->getSecret(),
            $request->input('sso_token'),
            $request->input('user_id')
        );

        return $this->hasher->check($checksumInput, $request->input('checksum'));
    }

    /**
     * Get the logout URL of the server.
     * 
     * @param string $returnUrl
     * @return void
     */
    public function getLogoutUrl(string $returnUrl)
    {
        return $this->serverUrl.'/sso/logout?return_url=' . $returnUrl;
    } 
}