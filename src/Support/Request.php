<?php

namespace Dottystyle\Laravel\SSO\Support;

use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Str;

final class Request
{
    /**
     * Determine whether the request accepts image as response via Accept header.
     * 
     * @param \Illuminate\Http\Request $request
     * @return boolean
     */
    public static function acceptsImage(HttpRequest $request)
    {
        $type = $request->getAcceptableContentTypes()[0] ?? '';

        return Str::contains($type, 'image/');
    }
}