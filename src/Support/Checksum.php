<?php

namespace Dottystyle\Laravel\SSO\Support;

final class Checksum
{
    /**
     * Make the input string for generating broker login request checksum.
     * 
     * @param string $token
     * @param mixed $brokerId
     * @param string $secret
     * @param string $extra
     * @return string
     */
    public static function makeLoginRequestInput(string $token, $brokerId, string $secret, string $extra = '')
    {
        return 'login_request'.$token.$brokerId.$secret.$extra;
    }

    /**
     * Make the input string for successful login response checksum.
     * 
     * @param mixed $brokerId
     * @param string $secret
     * @param string $sid
     * @param mixed $userId
     * @param string $extra (optional)
     * @return string
     */
    public static function makeLoginSuccessInput($brokerId, string $secret, string $sid, $userId, string $extra = '')
    {
        return 'login_receive'.$brokerId.$secret.$sid.$userId.$extra;
    }

    /**
     * Make the input string for failed login checksum.
     * 
     * @param mixed $brokerId
     * @param string $secret
     * @return string
     */
    public static function makeLoginFailedInput($brokerId, string $secret) 
    {
        return 'login_failed'.$brokerId.$secret;
    }

    /**
     * Make the input string for logout request.
     * 
     * @param mixed $brokerId
     * @param string $secret
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    public static function makeLogoutRequestInput($brokerId, string $secret, string $token, $userId)
    {
        return 'logout_'.$brokerId.$secret.$token.$userId;
    }

    /**
     * Make the input string for logout request.
     * 
     * @param mixed $brokerId
     * @param string $secret
     * @param string $token
     * @param mixed $userId
     * @return string
     */
    public static function makeLogoutResponseInput($brokerId, string $secret, string $token, $userId)
    {
        return 'logout_success_'.$brokerId.$secret.$token.$userId;
    }
}