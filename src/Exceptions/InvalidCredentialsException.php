<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class InvalidCredentialsException extends SSOException
{
}