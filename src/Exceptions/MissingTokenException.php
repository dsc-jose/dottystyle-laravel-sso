<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class MissingTokenException extends SSOException
{
}