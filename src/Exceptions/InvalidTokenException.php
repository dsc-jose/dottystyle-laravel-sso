<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class InvalidTokenException extends SSOException
{
}