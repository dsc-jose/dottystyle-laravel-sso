<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class AuthenticationException extends SSOException
{
}