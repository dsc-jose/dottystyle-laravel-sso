<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class GetUserInfoException extends SSOException
{
}