<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class AlreadyLoggedInException extends SSOException
{
}