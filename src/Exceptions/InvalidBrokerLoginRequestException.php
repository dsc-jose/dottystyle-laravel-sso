<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class InvalidBrokerLoginRequestException extends SSOException
{
}