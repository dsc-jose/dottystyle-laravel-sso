<?php

namespace Dottystyle\Laravel\SSO\Exceptions;

class MismatchedAuthAndTokenUsersException extends AuthenticationException
{
}