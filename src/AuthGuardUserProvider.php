<?php

namespace Dottystyle\Laravel\SSO;

use Dottystyle\Laravel\SSO\Contracts\UserProvider;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Support\Arrayable;
use InvalidArgumentException;

/**
 * Use the user provider of the default authentication guard.
 */
class AuthGuardUserProvider implements UserProvider 
{
    /**
     * @var \Illuminate\Contracts\Auth\UserProvider
     */
    protected $provider; 

    /**
     * @param \Illuminate\Cpntracts\Auth\Guard $guard
     */
    public function __construct(Guard $guard) 
    {
        // Must use the GuardHelpers trait or implement methods
        if (!method_exists($guard, 'getProvider')) {
            throw new InvalidArgumentException("Auth guard must implement getProvider method");
        }

        $this->provider = $guard->getProvider();
    }

    /**
     * Retrieve the user info by id.
     * 
     * @param mixed $id
     * @return \Dottystyle\Laravel\SSO\UserInfo
     */
    public function retrieveById($id) 
    {
        $user = $this->provider->retrieveById($id);
        
        if ($user instanceof Arrayable) {
            return new UserInfo($user->toArray());
        } else {
            return new UserInfo(array('id' => $user->getId()));
        }
    }
}