<?php

namespace Dottystyle\Laravel\SSO\Concerns;

use Dottystyle\Laravel\SSO\Exceptions\AuthenticationException;
use Dottystyle\Laravel\SSO\Exceptions\MismatchedAuthAndTokenUsersException;
use Exception;
use Closure;

/**
 * 
 */
trait ChecksAuthAndToken
{
    /**
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * @var \Illuminate\Contracts\Cookie\QueueingFactory
     */
    protected $cookies;
    
    /**
    * Log the user out of the application.
    * This will also clear the SSO token from cookie since it has no use.
    * 
    * @param  \Illuminate\Http\Request  $request
    * @return void
    */
    protected function logoutUser($request)
    {
        if ($this->auth->check()) {
            $this->auth->logout();

            $request->session()->invalidate();
        }

        $cookie = $this->cookies->forget($this->getTokenName());
        $this->cookies->queue($cookie);
    }

    /**
     * Get the token from request.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function getRequestToken($request)
    {
        return $request->cookies->get($this->getTokenName());
    }

    /**
     * Validates the auth user id is the same with the token user id.
     * 
     * @param mixed $tokenUserId
     * @return boolean
     */
    protected function validateSameUser($tokenUserId)
    {
        if ($this->auth->check() && $this->auth->userId() != $tokenUserId) {
            throw new MismatchedAuthAndTokenUsersException();
        }
    }
}