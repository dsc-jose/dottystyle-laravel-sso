<?php

namespace Dottystyle\Laravel\SSO;

use GuzzleHttp\Client as HttpClient;

trait SendsHttpRequests
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;
    
    /**
     * Get the HTTP client.
     * 
     * @return \GuzzleHttp\Client
     */
    public function getHttpClient()
    {
        if (!isset($this->httpClient)) {
            $this->httpClient = new HttpClient($this->getHttpClientOptions()); 
        }

        return $this->httpClient;
    }

    /**
     * Get the options when creating http client instance.
     * 
     * @return array
     */
    protected function getHttpClientOptions()
    {
        return [];
    }

    /**
     * Set the HTTP client instance.
     * 
     * @return void
     */
    public function setHttpClient(HttpClient $client) 
    {
        $this->httpClient = $client;
    }
}