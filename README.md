
## Server Setup

1. Enable the SSO server by registering the service provider
```php
<?php

    'providers' => [
        ...
        /*
         * Package Service Providers...
         */
        Dottystyle\Laravel\SSO\Server\SSOServiceProvider::class,
        ...
    ]

```

2. Publish config file by running `php artisan vendor:publish` then selecting the SSO package

3. Add or update the middleware priorities on App\Http\Kernel class. We need the SSO server middleware to be fired before the auth middleware. The broker middleware class should be `Dottystyle\Laravel\SSO\Server\Middleware\StartServerSession`.

```php
<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * Responsible for prioritizing the middleware
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Dottystyle\Laravel\SSO\Server\Middleware\StartServerSession::class,
        ...
    ];
```
    If cookies are encrypted, we need to add the token name on the exception list.

```php
<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        config('sso.token')
    ];
}
```


---



## Broker Setup (Service Provider)

1. Publish config file by running `php artisan vendor:publish` then selecting the SSO package

2. Change any desired or necessary configuration values

3. Use the `LoginsUnauthenticatedUsers` trait to the exception handler class (usually resides on app\Exception\Handler.php) to redirect users to the SSO login route if the app throws an AuthenticationException.

```php
<?php

namespace App\Exceptions;

use \Dottystyle\Laravel\SSO\Broker\LoginsUnauthenticatedUsers;

class Handler extends ExceptionHandler
{
    use LoginsUnauthenticatedUsers;
```

4. Add or update the middleware priorities on App\Http\Kernel class. We need the SSO broker middleware to be fired before the auth middleware. The broker middleware class should be `Dottystyle\Laravel\SSO\Broker\Middleware\StartBrokerSession`.

```php
<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * Responsible for prioritizing the middleware
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Dottystyle\Laravel\SSO\Broker\Middleware\StartBrokerSession::class,
        ...
    ];
```
    If cookies are encrypted, we need to add the token name on the exception list.

```php
<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        config('sso.token')
    ];
}
```